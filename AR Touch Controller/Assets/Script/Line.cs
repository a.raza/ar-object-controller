﻿using UnityEngine;

public class Line : MonoBehaviour
{
    public Transform m_demoObj;
    public float lineWidth = 0.2f, m_sen;
    public string sortingLayer;
    public Circle circle;
    public Transform[] TouchObjs;
    public LineRenderer lineRenderers;
    public AxisOption axis;

    /// /////////////////////////////////////////

    public float m_firstAngle, m_currentAngle, m_finalAngle, m_direction, m_axis;
    private float m_firstTouch;
    private float m_firstTouchAngle;
    private float m_secondTouchAngle;
    private const float m_default = 1000;


    protected virtual void Start()
    {
        switch (axis)
        {
            case AxisOption.X:
                m_axis = m_demoObj.eulerAngles.x;
                break;
            case AxisOption.Y:
                m_axis = m_demoObj.eulerAngles.y;
                break;
            default:
                m_axis = m_demoObj.eulerAngles.z;
                break;
        }


        TouchObjs[0].position = new Vector3(-circle.radius, 0f, 0f);
        TouchObjs[1].position = new Vector3(circle.radius, 0f, 0f);

        lineRenderers.SetPosition(0, TouchObjs[0].position);
        lineRenderers.SetPosition(1, TouchObjs[1].position);
        lineRenderers.enabled = false;

        ResetValues();
    }
    
    protected virtual void Update()
    {
        if (Input.touchCount == 2)
        {
            Touch t1 = Input.GetTouch(0);
            Touch t2 = Input.GetTouch(1);

            if (t1.phase == TouchPhase.Moved && t2.phase == TouchPhase.Moved)
            {
                SetupBeam(lineRenderers, t1.position, true);
                SetupBeam(lineRenderers, t2.position, false);
            }

            if (t1.phase == TouchPhase.Ended && t2.phase == TouchPhase.Ended || t1.phase == TouchPhase.Began && t2.phase == TouchPhase.Began)
            {
                ResetValues();
            }
        }
    }

    protected virtual void SetupBeam(LineRenderer lineRenderer, Vector3 pos, bool isLeft)
    {
        lineRenderer.enabled = true;

        if (isLeft)
        {
            var leftPos = PlaceOnCircle(pos, true, 1);
            TouchObjs[0].position = leftPos;
            //Debug.Log("First Interaction");
            lineRenderer.SetPosition(0, leftPos);
        }
        else
        {
            var rightPos = PlaceOnCircle(pos, false, 2);
            TouchObjs[1].position = rightPos;
            //Debug.Log("Second Interaction");
            lineRenderer.SetPosition(1, rightPos);
        }
    }

    private Vector3 PlaceOnCircle(Vector3 position, bool firstInteraction, float value)
    {
        Ray ray = Camera.main.ScreenPointToRay(position);

        Vector3 pos = ray.GetPoint(0f);

        pos = transform.InverseTransformPoint(pos);

        float angle = Mathf.Atan2(pos.x, pos.y) * Mathf.Rad2Deg;

        if (m_firstTouch == m_default)
            m_firstTouch = GetTriggerValue(firstInteraction, angle, pos.x, pos.y);
        
        if(m_firstTouch == value)
        {
            m_currentAngle = Mathf.Atan2(pos.x, pos.y).ToDeg();

            DirectionDetector();

            RotateSelectedObj();
        }
        
        return pos.Get2DCoordinates(circle.radius, angle);
    }

    private float GetTriggerValue(bool firstInteraction, float angle, float x, float y)
    {
        if (firstInteraction && m_firstTouchAngle == m_default)                // Check if the first input value is avaliable...
        {
            m_firstTouchAngle = Mathf.Atan2(x, y).ToDeg();          // get the first input value in angle
            

        }
        else if (!firstInteraction && m_secondTouchAngle == m_default)         // Check if the second input value is avaliable...
        {
            m_secondTouchAngle = Mathf.Atan2(x, y).ToDeg();         // get the second input value in angle
            
        }
        else    // compare which value is better suitable to measure and apply the rotation
        {
            if (m_firstTouch == m_default)  // if the suitable angle is not available...
            {
                m_firstAngle = m_firstTouchAngle > 70.0f && m_firstTouchAngle < 270.0f ? m_firstTouchAngle : m_secondTouchAngle; 

                return m_firstTouchAngle > 70.0f && m_firstTouchAngle < 270.0f ? (m_firstTouch = 1) : (m_firstTouch = 2);
            }
        }

        return m_firstTouch;
    }

    private void DirectionDetector()
    {
        m_direction = m_currentAngle > m_firstAngle ? 1 : -1; // get the direction in which the user is rotating

        m_finalAngle = m_firstAngle > m_currentAngle ? (m_firstAngle - m_currentAngle) / m_direction : (m_currentAngle - m_firstAngle) / m_direction;

        if (m_finalAngle.ToString()[0] == '-')
        {
            var finalAngle = 360 + m_finalAngle;

            m_finalAngle = finalAngle;
        }
    }

    private void RotateSelectedObj()
    {
        var eular = m_demoObj.transform.eulerAngles;
        var x = eular.x;
        var y = eular.y;
        var z = m_axis + m_finalAngle;

        Debug.Log("## Angle: " + (int)m_finalAngle);
        Debug.Log("## Object z: " + (int)eular.z);

        Debug.Log("## z: " + (int)z);

        var axis = z * m_sen * Mathf.Rad2Deg;
        m_demoObj.Rotate(Vector3.forward, axis, Space.World);

        //m_demoObj.transform.Rotate(x, y, z, Space.World);
    }

    private void ResetValues()
    {
        m_firstAngle = m_default;
        m_firstTouchAngle = m_default;
        m_secondTouchAngle = m_default;
        m_firstTouch = m_default;
        m_currentAngle = m_default;
        m_finalAngle = m_default;
        m_direction = m_default;
    }
}

//public enum Axis
//{
//    X,
//    Y,
//    Z
//};
