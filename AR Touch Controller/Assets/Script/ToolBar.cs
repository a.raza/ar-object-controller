﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ToolBar : TouchController
{
    [SerializeField] private GameObject m_options;//, m_rotationDisplay;
    [SerializeField] private TextMeshProUGUI m_scaleOMeter;
    [SerializeField] private Button m_scale, m_rotation, m_position;

    protected override void Start()
    {
        base.Start();

        OnDeselect += Deselect;
        OnSelect += Select;

        m_options = transform.GetChild(0).gameObject;
        m_options.SetActive(false);
        PrepareInfoHolder();
        PrepareOnScreenUI();
        SetOptionsColors(Control.NONE);
    }

    protected override void Scale()
    {
        var _scale = m_selectedObj.localScale;

        DisplayInfo("Scale", _scale);

        SetOptionsColors(Control.SCALE);

        base.Scale();
    }

    protected override void Rotate()
    {
        SetOptionsColors(Control.ROTATION);

        base.Rotate();
    }

    protected override void Move()
    {
        SetOptionsColors(Control.POSITION);
        
        base.Move();
    }

    #region Prepare Controller
    /// <summary>
    /// Following will display the percentage as according to the scale of the selected object
    /// </summary>
    private void PrepareInfoHolder()
    {
        var path = "[Footer]/[ScaleOMeter]";
        m_scaleOMeter = transform.GetTmpText(path);

        m_selectedObj = new GameObject("[SelectionHolder]").transform;

        //var rotationImg = "[RotationDisplay]";
        //m_rotationDisplay = transform.Find(rotationImg).gameObject;
        //m_rotationDisplay.SetActive(false);
    }

    private void PrepareOnScreenUI()
    {
        var container = m_options.transform;

        m_scale = new Button(container, Control.SCALE);
        m_rotation = new Button(container, Control.ROTATION);
        m_position = new Button(container, Control.POSITION);

        m_scale.m_button.onClick.AddListener(delegate { OnSelectOption(1); });
        m_rotation.m_button.onClick.AddListener(delegate { OnSelectOption(2); });
        m_position.m_button.onClick.AddListener(delegate { OnSelectOption(3); });
    }

    private void SetOptionsColors(Control control)
    {
        m_scale.m_bgImage.color = Color.white;
        m_rotation.m_bgImage.color = Color.white;
        m_position.m_bgImage.color = Color.white;
        m_scaleOMeter.transform.parent.gameObject.SetActive(false);

        switch (control)
        {
            case Control.POSITION:
                m_position.m_bgImage.color = Color.green;
                break;
            case Control.ROTATION:
                m_rotation.m_bgImage.color = Color.green;
                break;
            case Control.SCALE:
                m_scale.m_bgImage.color = Color.green;
                m_scaleOMeter.transform.parent.gameObject.SetActive(true);
                break;
        }
    }

    private void OnSelectOption(int selection)
    {
        // if the option is selected again, it will turn off that certain option
        if (selection == (int)m_control)
        {
            m_control = Control.NONE;
        }
        else
        {
            m_control = (Control)selection;
        }
    }

    private void DisplayInfo(string title, Vector3 value)
    {
        var _scale = (value.magnitude / 10).ToString("0.00");
        var info = title + ": " + _scale + "%";
        m_scaleOMeter.text = info;
    }

    private void Select()
    {
        m_options.SetActive(true);
    }

    private void Deselect()
    {
        SetOptionsColors(Control.NONE);
        m_options.SetActive(false);
        m_scaleOMeter.text = "";
    }

    private void OnDisable()
    {
        Deselect();
    }
    #endregion
}

[System.Serializable]
public class Button
{
    public UnityEngine.UI.Button m_button;
    public Image m_bgImage;

    public Button(Transform container, Control control)
    {
        if (container == null)
        {
            Debug.LogError("Er: Container Not Available");
            return;
        }

        this.m_button = container.Find(control.ToString()).GetComponent<UnityEngine.UI.Button>();
        this.m_bgImage = m_button.image;
    }
}
