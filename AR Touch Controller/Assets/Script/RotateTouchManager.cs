﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateTouchManager : MonoBehaviour
{
    private const float m_default = 1000;
    public AxisOption m_axisOption;
    public Transform m_selectedObj;
    public LineRenderer lineRenderer;
    public Transform[] m_touch;
    public float m_sen;
    public Circle m_circle;
    public float m_firstAngle, m_currentAngle, m_finalAngle, m_axis;
    private bool m_firstInteraction = false;

    protected virtual void Start()
    {
        GetAxis();
        ResetValues();
    }

    protected virtual void Update()
    {
        if (Input.touchCount == 2)
        {
            Touch t1 = Input.GetTouch(0);
            Touch t2 = Input.GetTouch(1);

            if (t1.phase == TouchPhase.Ended || t1.phase == TouchPhase.Began)
            {
                GetAxis();
                ResetValues();
            }
            else if (t1.phase == TouchPhase.Moved)
            {
                m_touch[0].position = PlaceOnCircle(t1.position);
                m_touch[1].position = PlaceOnCircle(t2.position);

                lineRenderer.SetPosition(0, m_touch[0].position);
                lineRenderer.SetPosition(1, m_touch[1].position);

                CalculateAngle(t1.position);
            }
        }
    }
    
    private void CalculateAngle(Vector3 position)
    {
        Ray ray = Camera.main.ScreenPointToRay(position);

        Vector3 pos = ray.GetPoint(0f);

        pos = transform.InverseTransformPoint(pos);

        float angle = Mathf.Atan2(pos.x, pos.y).ToDeg();

        if (m_firstInteraction == false)
        {
            m_firstAngle = Mathf.Atan2(pos.x, pos.y).ToDeg();

            m_firstInteraction = true;
        }

        m_currentAngle = Mathf.Atan2(pos.x, pos.y).ToDeg();

        DirectionDetector();

        RotateSelectedObj();
    }

    private Vector3 PlaceOnCircle(Vector3 position)
    {
        Ray ray = Camera.main.ScreenPointToRay(position);
        Vector3 pos = ray.GetPoint(0f);
        pos = transform.InverseTransformPoint(pos);
        float angle = Mathf.Atan2(pos.x, pos.y) * Mathf.Rad2Deg;
        return pos.Get2DCoordinates(m_circle.radius, angle);
    }

    private void DirectionDetector()
    {
        m_finalAngle = m_firstAngle > m_currentAngle ? 
            (m_firstAngle - m_currentAngle) / 1 : 
            (m_currentAngle - m_firstAngle) / -1;

        if (m_finalAngle.ToString()[0] == '-')
        {
            var finalAngle = 360 + m_finalAngle;

            m_finalAngle = finalAngle;
        }
    }

    private void RotateSelectedObj()
    {
        var eular = m_selectedObj.rotation;
        var x = eular.x;
        var y = eular.y;
        var z = m_axis + m_finalAngle;
        
        m_selectedObj.rotation = Quaternion.Euler(x, y, z);
    }

    private void ResetValues()
    {
        m_firstAngle = m_default;
        m_firstInteraction = false;
        m_currentAngle = m_default;
        m_finalAngle = m_default;
    }

    private void GetAxis()
    {
        lineRenderer.SetPosition(0, m_touch[0].position);
        lineRenderer.SetPosition(1, m_touch[1].position);
        lineRenderer.enabled = true;

        switch (m_axisOption)
        {
            case AxisOption.X:
                m_axis = m_selectedObj.eulerAngles.x;
                break;
            case AxisOption.Y:
                m_axis = m_selectedObj.eulerAngles.y;
                break;
            case AxisOption.Z:
                m_axis = m_selectedObj.eulerAngles.z;
                break;
        }
    }
}

//public enum AxisOption
//{
//    X,
//    Y,
//    Z
//};