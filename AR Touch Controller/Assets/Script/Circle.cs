﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class Circle : MonoBehaviour
{
    private int vertexCount = 40;
    public float linewidth = 0.85f, radius;
    public bool circleFillscreen;
    internal Camera cam;

    private LineRenderer lineRenderer;

    private void Awake()
    {
        cam = Camera.main;
        lineRenderer = GetComponent<LineRenderer>();
        StartCoroutine(CreateCircle());
    }
    
    IEnumerator CreateCircle()
    {
        yield return new WaitForSeconds(3.0f);
        lineRenderer.widthMultiplier = linewidth;

        if (circleFillscreen)
        {
            radius = Vector3.Distance(cam.ScreenToWorldPoint(new Vector3(0f, cam.pixelRect.yMax, 0f)),
                cam.ScreenToWorldPoint(new Vector3(0f, cam.pixelRect.yMin, 0f))) * 0.5f - linewidth;
            
            //radius = Vector3.Distance(Camera.main.ScreenToWorldPoint(new Vector3(0f, Camera.main.pixelRect.yMax, 0f)),
            //    Camera.main.ScreenToWorldPoint(new Vector3(0f, Camera.main.pixelRect.yMin, 0f))) * 0.5f - linewidth;
        }

        float deltaTheta = (2f * Mathf.PI) / vertexCount;
        float theta = 0f;

        lineRenderer.positionCount = vertexCount;

        for (int i = 0; i < lineRenderer.positionCount; i++)
        {
            Vector3 pos = new Vector3(radius * Mathf.Cos(theta) + cam.transform.position.x, radius * Mathf.Sin(theta) + cam.transform.position.y, 0f);

            lineRenderer.SetPosition(i, pos);
            theta += deltaTheta;
        }

        //StartCoroutine(CreateCircle());
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        var deltaTheta = (2f * Mathf.PI) / vertexCount;
        var theta = 0f;
        var cam = Camera.main;
        Vector3 oldPos = Vector3.zero;
        for (int i = 0; i < vertexCount + 1; i++)
        {
            Vector3 pos = new Vector3(radius * Mathf.Cos(theta) + cam.transform.position.x, radius * Mathf.Sin(theta) + cam.transform.position.y, 0f);
            Gizmos.DrawLine(oldPos, transform.position + pos);
            oldPos = transform.position + pos;
            theta += deltaTheta;
        }
    }
#endif
}
