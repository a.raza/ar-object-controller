﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;
using System.Linq;

public class TouchController : MonoBehaviour
{
    [Header("Max Distance Between Camera & Selected Object")]
    public float m_maxDistance = 40.0f;

    protected Control m_control; 
    protected Camera m_camera;
    protected Transform m_selectedObj;
    protected Touch m_hit;

    protected delegate void DlgOnDeselect();
    protected DlgOnDeselect OnDeselect;

    protected delegate void DlgOnSelect();
    protected DlgOnSelect OnSelect;

    #region Fields For Scaling Object
    [Header("For Scaling")]
    public float m_scaleSensitivity = 0.01f;
    private Vector3 m_viewPort, m_bottomLeft, m_topRight;
    private float m_distance, m_depth, m_tapTime = 0.03f, m_measuringTime = 1.0f, m_max;
    #endregion

    #region Fields For Rotating Object
    [Header("For Rotating")]
    public AxisOption m_axisOption;
    private float m_firstAngle, m_currentAngle, m_finalAngle, m_axis, m_linewidth = 0.85f, m_radius;
    private Transform m_tmp;
    private const float m_default = 1000;
    private bool m_firstInteraction = false;
    #endregion

    #region Fields For Moving Object
    [Header("For Moving")]
    private Camera m_tmpCam;
    private Plane m_objPlane;
    private Vector3 m_move;
    #endregion


    protected virtual void Start()
    {
        m_camera = Camera.main;
        m_control = Control.NONE;

        // This is related to rotation & Movement
        m_tmp = new GameObject("[TMP]").transform;
        m_tmpCam = m_tmp.gameObject.AddComponent<Camera>();
        m_tmpCam.orthographic = true;
        m_tmpCam.enabled = false;

        GetRadius();

        // This is related to Scaling
        StartCoroutine(MeasureCameraDistance());
    }

    protected virtual void Update()
    {
        switch (m_control)
        {
            case Control.ROTATION:
                Rotate();
                break;
            case Control.SCALE:
                Scale();
                break;
            case Control.POSITION:
                Move();
                break;
        }

        SelectionMode();
    }
    
    #region Methods To Handle Options/Object Selection
    /// <summary>
    /// Following method will allow user to jump for one selected object to another with one click
    /// And would also allow user to unselect the current selected object with double click
    /// </summary>
    protected virtual void SelectionMode()
    {
        if (EventSystem.current.currentSelectedGameObject == null) // if not clicking on any UI
        {
            if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Began)
            {
                m_hit = Input.GetTouch(0);
                StartCoroutine(SingleOrDouble());
            }
        }
    }

    /// <summary>
    /// Following fucntion will check taps on the screen and will trigger certain function accordingly
    /// </summary>
    /// <returns></returns>
    private IEnumerator SingleOrDouble()
    {
        yield return new WaitForSeconds(m_tapTime);

        switch (m_hit.tapCount) 
        {
            case 1: //one tap will select an object

                Debug.Log("Single TAP");
                
                Ray raycast = Camera.main.ScreenPointToRay(m_hit.position);

                if (Physics.Raycast(raycast, out RaycastHit raycastHit))
                {
                    Debug.Log("Raycast Hit");

                    if (raycastHit.collider.gameObject.tag == "Interactable") // Only get the objects that are Interactable
                    {
                        var _hitObj = raycastHit.collider.gameObject.transform;

                        EmptySelectionHolder();

                        Debug.Log("Selected: " + _hitObj.name);
                        m_selectedObj.localScale = _hitObj.localScale.GetProperVector3();
                        m_selectedObj.position = _hitObj.position;

                        _hitObj.SetParent(m_selectedObj);
                        _hitObj.localPosition = Vector3.zero;

                        OnSelect?.Invoke();
                    }
                }

                break;

            case 2: // Tapping twice on the anywhere on the scene will deselect the current selected object
                
                Debug.Log("Double TAP");
                StartCoroutine(SingleOrDouble());
                OnDeselect?.Invoke();
                EmptySelectionHolder();
                m_control = Control.NONE;
                break;
        }
    }

    /// <summary>
    /// Following will remove the selected object form the "Selection Holder"
    /// </summary>
    private void EmptySelectionHolder()
    {
        if (m_selectedObj.childCount != 0)
        {
            var _obj = m_selectedObj.GetChild(0).transform;
            _obj.SetParent(null);
            _obj.position = m_selectedObj.position;
        }
    }
    #endregion

    #region Method to Scale, Rotate, Move
    protected virtual void Scale()
    {
        var _scale = m_selectedObj.localScale;

        if (Input.touchCount == 2)
        {
            var t1 = Input.GetTouch(0);
            var t2 = Input.GetTouch(1);

            var t1PrevPos = t1.position - t1.deltaPosition;
            var t2PrevPos = t2.position - t2.deltaPosition;

            var prevMagnitude = (t1PrevPos - t2PrevPos).magnitude;
            var currentMagnitude = (t1.position - t2.position).magnitude;

            var dis = currentMagnitude - prevMagnitude;
            
            var x = _scale.x;
            var y = _scale.y;
            var z = _scale.z;
            
            var newScale = new Vector3
                (
                    Mathf.Clamp(x - (-dis) * m_scaleSensitivity, 1, m_max),
                    Mathf.Clamp(y - (-dis) * m_scaleSensitivity, 1, m_max),
                    Mathf.Clamp(z - (-dis) * m_scaleSensitivity, 1, m_max)
               );

            m_selectedObj.localScale = newScale;
        }
    }

    protected virtual void Rotate()
    {
        m_tmp.position = Vector3.zero;

        if (Input.touchCount == 2)
        {
            Touch t1 = Input.GetTouch(0);
            Touch t2 = Input.GetTouch(1);

            if (t1.phase == TouchPhase.Ended || t1.phase == TouchPhase.Began)
            {
                GetAxis();
                ResetValues();
            }
            else if (t1.phase == TouchPhase.Moved)
            {
                CalculateAngle(t1.position);
            }
        }
    }

    protected virtual void Move()
    {
        if (Input.touchCount == 1)
        {
            Touch t = Input.GetTouch(0);

            if (t.phase == TouchPhase.Began)
            {
                m_objPlane = new Plane(m_camera.transform.forward * -1, transform.position);

                Ray tRay = m_camera.ScreenPointToRay(t.position);
                m_objPlane.Raycast(tRay, out float rayDistance);
                m_move = m_selectedObj.position - tRay.GetPoint(rayDistance);
            }
            else if (t.phase == TouchPhase.Moved)
            {
                Ray tRay = m_camera.ScreenPointToRay(t.position);
                if (m_objPlane.Raycast(tRay, out float rayDistance))
                {
                    m_selectedObj.position = tRay.GetPoint(rayDistance) + m_move;
                }
            }
        }

        //if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonUp(0))
        //{
        //    var m = Input.mousePosition;

        //    m_objPlane = new Plane(m_camera.transform.forward * -1, transform.position);

        //    Ray tRay = m_camera.ScreenPointToRay(m);
        //    m_objPlane.Raycast(tRay, out float rayDistance);
        //    m_move = transform.position - tRay.GetPoint(rayDistance);
        //}
        //else if (Input.GetMouseButton(0))
        //{
        //    var m = Input.mousePosition;
        //    Ray tRay = m_camera.ScreenPointToRay(m);
        //    if (m_objPlane.Raycast(tRay, out float rayDistance))
        //    {
        //        transform.position = tRay.GetPoint(rayDistance) + m_move;
        //    }
        //}
    }
    #endregion

    #region Measure Radius
    /// <summary>
    /// Following will get the radius as according to the screen size
    /// </summary>
    void GetRadius()
    {
        ///following method will put the camera to the orthographic point-of-view only to get the proper screen size,
        /// and then will turn the camera point-of-view to its previous state

        //if (m_camera.orthographic == false)
        //    m_camera.orthographic = true;

        m_radius = Vector3.Distance(Camera.main.ScreenToWorldPoint(new Vector3(0f, m_tmpCam.pixelRect.yMax, 0f)),
            Camera.main.ScreenToWorldPoint(new Vector3(0f, m_tmpCam.pixelRect.yMin, 0f))) * 0.5f - m_linewidth;

        //m_camera.orthographic = false;

    }
    #endregion
    
    #region Methods To Handle Scale
    /// <summary>
    /// Following function will measure the distance every second and will update and determine the 'max' value of the scale
    /// </summary>
    /// <returns></returns>
    private IEnumerator MeasureCameraDistance()
    {
        yield return new WaitForSeconds(m_measuringTime);

        m_depth = (m_camera.transform.position.z * m_maxDistance) / 100;

        if (m_selectedObj != null)
        {
            m_distance = (m_depth * 0.5f);

            m_viewPort.Set(0, 0, m_distance);
            m_bottomLeft = Camera.main.ViewportToWorldPoint(m_viewPort);

            m_viewPort.Set(1, 1, m_distance);
            m_topRight = Camera.main.ViewportToWorldPoint(m_viewPort);

            var max = new Vector3(m_bottomLeft.x - m_topRight.x, m_bottomLeft.y - m_topRight.y, m_depth);

            m_max = max.magnitude; //Found Max As according to the size of the screen 

        }

        StartCoroutine(MeasureCameraDistance());
    }
    #endregion

    #region Methods To Handle Rotation

    /// <summary>
    /// This will aid in rotating the selected object in certain direction
    /// </summary>
    private void GetAxis()
    {
        switch (m_axisOption)
        {
            case AxisOption.X:
                m_axis = m_selectedObj.eulerAngles.x;
                break;
            case AxisOption.Y:
                m_axis = m_selectedObj.eulerAngles.y;
                break;
            case AxisOption.Z:
                m_axis = m_selectedObj.eulerAngles.z;
                break;
        }
    }

    /// <summary>
    /// This will reset the values that aid in calculating the rotation
    /// </summary>
    private void ResetValues()
    {
        m_firstAngle = m_default;
        m_firstInteraction = false;
        m_currentAngle = m_default;
        m_finalAngle = m_default;
    }

    /// <summary>
    /// Following will get the position of the touch from the user,
    /// And will calculate the angle in 360° from first interaction position to the last interaction position
    /// </summary>
    /// <param name="position"></param>
    private void CalculateAngle(Vector3 position)
    {
        var camPos = new Vector3(m_camera.transform.position.x, m_camera.transform.position.y, 0f);
        var ray = Camera.main.ScreenPointToRay(position);
        var pos = ray.GetPoint(0f) - camPos;
        pos = m_tmp.InverseTransformPoint(pos);
        var angle = Mathf.Atan2(pos.x, pos.y).ToDeg();

        if (m_firstInteraction == false)
        {
            m_firstAngle = angle;
            m_firstInteraction = true;
        }

        m_currentAngle = angle;
        DirectionDetector();
        RotateObj();
    }

    /// <summary>
    /// In the follwing method the direction of the angle will get converted from (1° to -1°) to (1° to 360°)
    /// </summary>
    private void DirectionDetector()
    {
        m_finalAngle = m_firstAngle > m_currentAngle ?
            (m_firstAngle - m_currentAngle) / 1 :
            (m_currentAngle - m_firstAngle) / -1;

        // when the angle goes beyond -180° convert it into 360°
        if (m_finalAngle.IsNegative())
        {
            var finalAngle = 360 + m_finalAngle;

            m_finalAngle = finalAngle;
        }
    }

    /// <summary>
    /// Following will rotated the selected object accordingly
    /// </summary>
    private void RotateObj()
    {
        var eular = m_selectedObj.rotation;
        var x = 0f;
        var y = 0f;
        var z = 0f;

        switch (m_axisOption)
        {
            case AxisOption.X:
                x = m_axis + m_finalAngle;
                y = eular.y;
                z = eular.z;
                break;
            case AxisOption.Y:
                x = eular.x;
                y = m_axis + m_finalAngle;
                z = eular.z;
                break;
            case AxisOption.Z:
                x = eular.x;
                y = eular.y;
                z = m_axis + m_finalAngle;
                break;
        }

        m_selectedObj.rotation = Quaternion.Euler(x, y, z);
    }
    
    #endregion
}

public enum Control
{
    NONE = 0,
    SCALE = 1,
    ROTATION = 2,
    POSITION = 3,
};

public enum AxisOption
{
    X,
    Y,
    Z
};

public static class ExtensionMethods
{
    public static TextMeshProUGUI GetTmpText(this Transform trans, string pathInTransform)
    {
        return trans.Find(pathInTransform).GetComponent<TextMeshProUGUI>();
    }

    public static Vector3 GetProperVector3(this Vector3 value)
    {
        var values = new float[3];

        values[0] = value.x;
        values[1] = value.y;
        values[2] = value.z;

        var maxValue = values.Max();
        var maxIndex = values.ToList().IndexOf(maxValue);

        value = new Vector3(maxValue, maxValue, maxValue);

        return value;
    }

    public static float ToDeg(this float value)
    {
        var angle = (value * Mathf.Rad2Deg);

        if (angle.IsNegative())
        {
            angle = 360 - (angle / -1);
        }

        return angle;
    }

    public static Vector3 Get2DCoordinates(this Vector3 value, float radius, float angle)
    {
        value.x = radius * Mathf.Sin(angle * Mathf.Deg2Rad);        /// x = r * sin(0)
        value.y = radius * Mathf.Cos(angle * Mathf.Deg2Rad);        /// y = r * cos(0)
        value.z = 0f;

        return value;
    }

    public static bool IsNegative(this float value)
    {
        if (value.ToString()[0] == '-')
        {
            return true;
        }

        return false;
    }
}

